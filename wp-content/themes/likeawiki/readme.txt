Theme Name: LikeAWiki
Description: Looks like a wiki.
Author: Larry Judd Oliver - Tradesouthwest

== Additional License Information ==  
* HTML5 Shiv v3.7.3 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed    
* Image for default header by Larry Judd | @tradesouthwest | GPL2 Licensed

=== Notes ===
* Some content will disappear or change sizes at smaller screen width, such as tool bar and logo
* Some content will change position at smaller screen widths, such as log will move to bottom of screen
* There are some options in the functions file to change the way the breadcrumbs behave, like s"show breadcrumbs on home page" for example.
Pleas use a Child Theme if you choose to make these changes. OPTIONS that are editable are between |*==== OPTIONS ====*| markers.

== Change Log ==

= 0.1 =
Feb. 25th 2015
* first release
= 0.2 =
Mar. 6th, 2015
* fixed clear floats
* added pagination
* added styles to metadata and entry footers
* reworked "tools" div
* fixed some responsive screen calls
* made header smaller
* removed default background image
* added title attribute and title-tag support
* fixed screen reader text div
= 0.3 =
* moved theme support scripts inside of theme setup
= 0.4 =
* updated theme uri
= 0.5 =
* Aug 19th 2015
* fixed secondary navigation z-index
= 0.6 =
* Nov 27th 2015
* provided new uri for theme
= 0.7 =
* Nov 27th 2015
* fixed typos in text domain
= 0.8 =
* oct 27th 2015
* added style to post-date
* escaped multiple missed strings and outputs
  - functions.php, archives.php, header.php
* added missing quote around theme-options 'likeawiki_checkbox'
* various typos
= 0.9 =
* Oct 28th 2015
* fixed sanitizer for customize tools
* breadcrumbs issue fixed
* directory uri corrected