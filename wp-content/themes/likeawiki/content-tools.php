<section id="tool-bar">
    <div id="breadcrumbs">
        <?php likeawiki_breadcrumbs(); ?>
    </div>
        <nav class="toolbar">
            <div class="likeawiki-tools">
                <div class="aligninline">
                    <div id="tools">
                        <p><button type="button" class="button"><?php _e( 'Tools', 'likeawiki' ); ?></button><button type="button" class="button"><?php _e( 'OK', 'likeawiki' ); ?></button></p>
                             <div class="hidden">
                                 <div>
        <?php $options = get_option( 'likeawiki_theme_options' ); ?>
        <?php if ( 1 == get_theme_mod('checkbox_likeawiki') ) : ?>
        <ul>
        <?php if( !empty( $options['likeawiki_t1'] ) ) { ?><li>
        <?php echo esc_attr( $options['likeawiki_t1'] ); ?></li><?php } ?>
        <?php if( !empty( $options['likeawiki_t2'] ) ) { ?><li>
        <?php echo esc_attr( $options['likeawiki_t2'] ); ?></li><?php } ?>
        <?php if( !empty( $options['likeawiki_t3'] ) ) { ?> <li>
        <?php echo esc_attr( $options['likeawiki_t3'] ); ?></li><?php } ?>
        <?php if( !empty( $options['likeawiki_t4'] ) ) { ?><li>
        <?php echo esc_attr( $options['likeawiki_t4'] ); ?></li><?php } ?>
        <?php else : ?>
            <ul>
        <li><?php _e( 'To search for a word and highlight it on page use <span>Ctrl + F</span>', 'likeawiki' ); ?></li>
        <li><?php _e( 'To copy text from page, highlight text and then <span>Ctrl + C</span>', 'likeawiki' ); ?></li>
        <li><?php _e( 'To paste that text into another page, click page then <span>Ctrl + V</span>', 'likeawiki' ); ?></li>
        <li><?php _e( 'To search Internet, highlight text then right click <span>Options List</span>', 'likeawiki' ); ?></li>
        <?php endif; ?>
<li><?php _e( 'Current pixel font size is', 'likeawiki' ); ?> <span><?php echo esc_attr( $options['font_size'] ); ?></span></li>
&nbsp; <small> <?php _e( ' click OK to remove message', 'likeawiki' ); ?></small>
        </ul>   
                                </div>
                           </div>
                    </div><!--#tools-->
                </div>
            </div> 
        </nav>
</section>